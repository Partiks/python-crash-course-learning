alien_color="green"

if alien_color == "green":
    print(f"You earned 5 point! You shot down a green colored-alien!")
elif alien_color == "yellow":
    print(f"You earned 10 point! You shot down a yellow colored-alien!")
elif alien_color == "red":
    print(f"You earned 15 point! You shot down a red colored-alien!")
else:
    print(f"Alien color is not green")
