import math

guest_list = [ 'Aryabhatta', 'Jon Lennon', 'Albert Einstein', 'Slyvester Stallone', 'Ed Winters', 'Stephen Hawking' ]

print("We just found a bigger table!")

guest_list.insert(0, 'Buckminister Fuller')
middle = (math.floor(len(guest_list)/2))
#print(middle)
guest_list.insert(middle, 'Socrates')
guest_list.append('Galileo')
guest_list.append('Richard Feynman')


print(guest_list)

for i in guest_list:
    print(f"\nHey {i}, would you like to join me for a dinner for talking about the world's physics, eating habits, and morals?")
