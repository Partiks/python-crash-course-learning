guest_list = [ 'Jon Lennon', 'Albert Einstein', 'Slyvester Stallone', 'Ed Winters', 'Stephen Hawking' ]

print(f"Unfortunately due to shooting of a new Rocky/Creed film, {guest_list.pop(2)} can't make it.\n")
# Below doesn't work because remove is used to remove elements with their values and it doesn't return anything.
#removed_guest = guest_list.remove('Slyvester Stallone')
#print(f"Unfortunately due to shooting of a new Rocky/Creed film, {removed_guest} can't make it.")

guest_list.insert(2, 'Nelson Mandela')

print(guest_list)

for i in guest_list:
    print(f"\nHey {i}, would you please join us for a dinner to talk about the world's physics, eating habits, and morals today?")
