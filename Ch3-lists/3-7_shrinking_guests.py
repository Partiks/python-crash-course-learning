import math

guests = [ 'Jon Lennon', 'Albert Einstein', 'Aryabhatta', 'Slyvester Stallone', 'Ed Winters', 'Stephen Hawking', 'Cleopatra', 'Mother Teresa', 'Madam Curie' ]

print("We just found a bigger table!")

guests.insert(0, 'Buckminister Fuller')
middle = (math.floor(len(guests)/2))
#print(middle)
guests.insert(middle, 'Socrates')
guests.append('Galileo')
guests.append('Richard Feynman')


print(guests)

print("\nActually, new dining table won't arrive in time :(")

while len(guests) > 2:
    popped_guest = guests.pop()
    print(f"Very sorry, but we ran out of space Mx. {popped_guest}")

print(guests)

#for i in guests:
#    print(f"\nHey {i}, would you like to join me for a dinner for talking about the world's physics, eating habits, and morals?")

del guests[0]
del guests[0]

print(guests)
