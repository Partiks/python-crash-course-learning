guest_list = [ 'Jon Lennon', 'Albert Einstein', 'Slyvester Stallone', 'Ed Winters', 'Stephen Hawking' ]

for i in guest_list:
    print(f"\nHey {i}, would you like to join me for a dinner for talking about the world's physics, eating habits, and morals?")
