foods = ('bell peppers', 'hummus', 'romaine lettuce', 'onions', 'tomatoes')

print(f"{foods}")

#foods[2] = 'dal'# - This is not supported as tuples can't be edited.

# Available foods changed by chef

foods = ('bell peppers', 'hummus', 'romaine lettuce', 'lentils', 'beans')

for food in foods:
    print(f"{food}")
