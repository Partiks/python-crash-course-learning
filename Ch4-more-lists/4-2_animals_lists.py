tortured_animals_by_humans = [ 'cows', 'pigs', 'chickens', 'fish', 'octopuses' ]

for poor_soul in tortured_animals_by_humans:
    print(f"I feel empathy the way humans abuse and torture {poor_soul}")

print("\nImagine there wouldn't be any exploitation of wildlife and nature by humans\nYou may say that I'm a dreamer, but I am not alone...\n  - John Lennon")
